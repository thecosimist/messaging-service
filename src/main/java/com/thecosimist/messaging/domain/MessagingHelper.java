/**
 * Created with IntelliJ IDEA.
 * User: cosimospada
 * Date: 2016-06-08
 * Time: 22:12
 */
package com.thecosimist.messaging.domain;

import com.thecosimist.messaging.server.DbInitializer;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class MessagingHelper
{
    public static Message getMessage(String userId, Integer messageId)
    {
        try
        {
            return MessagingDao.getMessage(DbInitializer.getConnection(), userId, messageId);
        }
        catch (SQLException e)
        {
            e.printStackTrace();    // Currently not handled; all these should be handled and return 505/internal server error
        }

        return null;
    }

    public static List<Message> getAllMessages(String userId)
    {
        try
        {
            return MessagingDao.getAllMessages(DbInitializer.getConnection(), userId);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static List<Message> getMessagesSince(String userId, String startTimeString)
    {
        try
        {
            return MessagingDao.getMessagesSince(DbInitializer.getConnection(), userId, parse(startTimeString));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static List<Message> getPagedMessages(String userId, Integer startIndex, Integer endIndex)
    {
        try
        {
            return MessagingDao.getPagedMessages(DbInitializer.getConnection(), userId, startIndex, endIndex);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static List<Message> getMessagesByDateRange(String userId, String startTime, String endTime)
    {
        try
        {
            return MessagingDao.getMessagesByDateRange(DbInitializer.getConnection(), userId, parse(startTime), parse(endTime));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static void sendMessage(String userId, String receiverId, String messageText)
    {
        try
        {
            MessagingDao.sendMessage(DbInitializer.getConnection(), userId, receiverId, messageText);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public static void deleteMessages(String userId, Integer[] messageIds)
    {
        try
        {
            for (Integer messageId : messageIds) // Kinda ugly solution since there's no good way of writing IN statements in a prepared statement :-(
            {
                MessagingDao.deleteMessage(DbInitializer.getConnection(), userId, messageId);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private static LocalDateTime parse(String dateString)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(dateString, formatter);

        return dateTime;
    }
}
