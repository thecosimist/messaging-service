/**
 * Created with IntelliJ IDEA.
 * User: cosimospada
 * Date: 2016-05-29
 * Time: 23:45
 */
package com.thecosimist.messaging.domain;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessagingDao
{
    private MessagingDao()
    {
    }

    public static User getUserById(Connection connection, String userId) throws SQLException
    {
        String sql = "select * from USERS where USER_ID = ?";
        User user;

        try (PreparedStatement statement = connection.prepareStatement(sql)) // All PreparedStatements are in try clauses in order to auto-close them
        {
            statement.setString(1, userId);
            user = getSingleUser(statement);
        }

        return user;
    }

    public static List<User> getAllUsers(Connection connection) throws SQLException
    {
        String sql = "select * from USERS ";
        List<User> users;

        try (PreparedStatement statement = connection.prepareStatement(sql))
        {
            users = getUsers(statement);
        }

        return users;
    }

    public static List<Message> getAllMessages(Connection connection, String userId) throws SQLException
    {
        String sql = "select * from MESSAGES where RECEIVER_ID = ? order by SENT_DATE desc ";
        List<Message> messages;

        try (PreparedStatement statement = connection.prepareStatement(sql))
        {
            statement.setString(1, userId);
            messages = getMessages(statement);
        }

        return messages;
    }

    public static Message getMessage(Connection connection, String userId, Integer messageId) throws SQLException
    {
        String sql = "select * from MESSAGES where RECEIVER_ID = ? and MESSAGE_ID = ?";
        Message message;

        try (PreparedStatement statement = connection.prepareStatement(sql))
        {
            statement.setString(1, userId);
            statement.setInt(2, messageId);
            message = getSingleMessage(statement);
        }

        return message;
    }

    public static List<Message> getMessagesSince(Connection connection, String userId, LocalDateTime startTime) throws SQLException
    {
        String sql = "select * from MESSAGES where RECEIVER_ID = ? and SENT_DATE > ? order by SENT_DATE desc ";
        List<Message> messages;

        try (PreparedStatement statement = connection.prepareStatement(sql))
        {
            statement.setString(1, userId);
            statement.setTimestamp(2, Timestamp.valueOf(startTime));
            messages = getMessages(statement);
        }

        return messages;
    }

    public static List<Message> getPagedMessages(Connection connection, String userId, Integer startIndex, Integer endIndex) throws SQLException
    {
        String sql = "select * from MESSAGES where RECEIVER_ID = ? order by SENT_DATE desc OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        List<Message> messages;

        try (PreparedStatement statement = connection.prepareStatement(sql))
        {
            statement.setString(1, userId);
            statement.setInt(2, startIndex);
            statement.setInt(3, (endIndex - startIndex));
            messages = getMessages(statement);
        }

        return messages;
    }

    public static List<Message> getMessagesByDateRange(Connection connection, String userId, LocalDateTime startTime, LocalDateTime endTime)
        throws SQLException
    {
        String sql = "select * from MESSAGES where RECEIVER_ID = ? and SENT_DATE > ? and SENT_DATE < ? order by SENT_DATE desc ";
        List<Message> messages;

        try (PreparedStatement statement = connection.prepareStatement(sql))
        {
            statement.setString(1, userId);
            statement.setTimestamp(2, Timestamp.valueOf(startTime));
            statement.setTimestamp(3, Timestamp.valueOf(endTime));
            messages = getMessages(statement);
        }

        return messages;
    }

    public static void sendMessage(Connection connection, String userId, String receiverId, String messageText)
        throws SQLException
    {
        String sql = "insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values (?, ?, CURRENT_TIMESTAMP, ?)";

        try (PreparedStatement statement = connection.prepareStatement(sql))
        {
            statement.setString(1, userId);
            statement.setString(2, receiverId);
            statement.setString(3, messageText);

            statement.executeUpdate();
        }
    }

    public static void deleteMessage(Connection connection, String userId, Integer messageId)
        throws SQLException
    {
        String sql = "delete from MESSAGES where RECEIVER_ID = ? and MESSAGE_ID = ? ";

        try (PreparedStatement statement = connection.prepareStatement(sql))
        {
            statement.setString(1, userId);
            statement.setInt(2, messageId);

            statement.executeUpdate();
        }
    }

    //////////////////////////////////////////////////////
    // Utility methods for getting data from result sets

    private static Message getSingleMessage(PreparedStatement statement) throws SQLException
    {
        List<Message> messages = getMessages(statement);
        if (messages.size() == 1)
        {
            return messages.get(0);
        }

        return null;
    }

    private static List<Message> getMessages(PreparedStatement statement) throws SQLException
    {
        List<Message> messages = new ArrayList<>();

        try (ResultSet resultSet = statement.executeQuery())    // Auto-closes result set ....
        {
            while (resultSet.next())
            {
                Integer messageId = resultSet.getInt("MESSAGE_ID");
                String senderId = resultSet.getString("SENDER_ID");
                String receiverId = resultSet.getString("RECEIVER_ID");
                LocalDateTime sentDate = resultSet.getTimestamp("SENT_DATE").toLocalDateTime();
                String messageText = resultSet.getString("MESSAGE_TEXT");

                messages.add(new Message(messageId, senderId, receiverId, sentDate, messageText));
            }
        }

        return messages;
    }

    private static User getSingleUser(PreparedStatement statement) throws SQLException
    {
        List<User> users = getUsers(statement);
        if (users.size() == 1)
        {
            return users.get(0);
        }

        return null;
    }

    private static List<User> getUsers(PreparedStatement statement) throws SQLException
    {
        List<User> users = new ArrayList<>();

        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next())
        {
            String userId = resultSet.getString("USER_ID");
            String lastName = resultSet.getString("FIRST_NAME");
            String firstName = resultSet.getString("LAST_NAME");

            users.add(new User(userId, firstName, lastName));
        }

        return users;
    }
}
