/**
 * Created with IntelliJ IDEA.
 * User: cosimospada
 * Date: 2016-06-08
 * Time: 22:25
 */
package com.thecosimist.messaging.domain;

public class User
{
    String userId;
    String firstName;
    String lastName;

    public User(String userId, String firstName, String lastName)
    {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
}
