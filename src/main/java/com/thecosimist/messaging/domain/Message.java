/**
 * Created with IntelliJ IDEA.
 * User: cosimospada
 * Date: 2016-05-30
 * Time: 22:45
 */
package com.thecosimist.messaging.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Message
{
    private Integer messageId;
    private String senderId;
    private String receiverId;
    private LocalDateTime sentDate;
    private String messageText;

    public Message(Integer messageId, String senderId, String receiverId, LocalDateTime sentDate, String messageText)
    {
        this.messageId = messageId;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.sentDate = sentDate;
        this.messageText = messageText;
    }

    public Integer getMessageId()
    {
        return messageId;
    }

    public String getSenderId()
    {
        return senderId;
    }

    public String getReceiverId()
    {
        return receiverId;
    }

    public String getSentDate()
    {
        return sentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    public String getMessageText()
    {
        return messageText;
    }
}
