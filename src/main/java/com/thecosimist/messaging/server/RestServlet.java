/**
 * Created with IntelliJ IDEA.
 * User: cosimospada
 * Date: 2016-05-29
 * Time: 23:28
 */
package com.thecosimist.messaging.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thecosimist.messaging.domain.Message;
import com.thecosimist.messaging.domain.MessagingHelper;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

@Path("/")
public class RestServlet
{
    @Context
    UriInfo uriInfo;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String handleBasicGet()
    {
        return "Looking good! \n";
    }

    @Path("{userId}/read/{messageId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessage(@PathParam("userId") String userId, @PathParam("messageId") Integer messageId)
    {
        Message message = MessagingHelper.getMessage(userId, messageId);
        return Response.ok(jsonify(message), MediaType.APPLICATION_JSON).build();
    }

    @Path("{userId}/readall")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessages(@PathParam("userId") String userId)
    {
        List<Message> messages = MessagingHelper.getAllMessages(userId);
        return Response.ok(jsonify(messages), MediaType.APPLICATION_JSON).build();
    }

    @Path("{userId}/readsince/{startTime}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessagesSince(@PathParam("userId") String userId, @PathParam("startTime") String startTime)
    {
        List<Message> messages = MessagingHelper.getMessagesSince(userId, startTime);
        return Response.ok(jsonify(messages), MediaType.APPLICATION_JSON).build();
    }

    @Path("{userId}/readpage/{startIndex}/to/{endIndex}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPagedMessages(@PathParam("userId") String userId, @PathParam("startIndex") Integer startIndex,
        @PathParam("endIndex") Integer endIndex)
    {
        List<Message> messages = MessagingHelper.getPagedMessages(userId, startIndex, endIndex);
        return Response.ok(jsonify(messages), MediaType.APPLICATION_JSON).build();
    }

    @Path("{userId}/readfrom/{startTime}/to/{endTime}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessagesInDateRange(@PathParam("userId") String userId, @PathParam("startTime") String startTime,
        @PathParam("endTime") String endTime)
    {
        List<Message> messages = MessagingHelper.getMessagesByDateRange(userId, startTime, endTime);
        return Response.ok(jsonify(messages), MediaType.APPLICATION_JSON).build();
    }

    @Path("{userId}/send/{receiverId}/{messageText}")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Response sendMessage(@PathParam("userId") String userId, @PathParam("receiverId") String receiverId,
        @PathParam("messageText") String messageText)
    {
        MessagingHelper.sendMessage(userId, receiverId, messageText);
        return Response.created(createUserReadallUri(userId)).build(); // Status 201/Created
    }

    @Path("{userId}/delete/{messageIds}")
    @DELETE
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteMessages(@PathParam("userId") String userId, @PathParam("messageIds") String messageIds)
    {
        MessagingHelper.deleteMessages(userId, splitStringCsvToInts(messageIds));
        return Response.noContent().build();    // Status 204. REST responses: https://tools.ietf.org/html/rfc7231#section-4.3
    }

    //////////////////////////////////////////////////////
    // Utility methods

    private String jsonify(Object o)
    {
        ObjectMapper jsonMapper = new ObjectMapper();

        try
        {
            return jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(o) + "\n";
        }
        catch (JsonProcessingException e)
        {
            return null;
        }
    }

    public static Integer[] splitStringCsvToInts(String intString)
    {
        String[] intStrings = intString.split(",");
        Integer[] ints = Arrays.stream(intStrings).map(Integer::parseInt).toArray(Integer[]::new);  // lite lambda!
        return ints;
    }

    private Response buildValidationErrorResponse() // For reporting invalid input, e.g. if a user or a message doesn't exists
    {
        return Response.status(Response.Status.BAD_REQUEST).entity("Invalid user and/or message!").build();
    }

    private URI createUserReadallUri(String userId)
    {
        try
        {
            URL url = new URL(uriInfo.getBaseUri().toString() + userId + "/readall");
            return url.toURI();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
