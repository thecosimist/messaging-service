/**
 * Created with IntelliJ IDEA.
 * User: cosimospada
 * Date: 2016-06-11
 * Time: 21:47
 */
package com.thecosimist.messaging.server;

import java.sql.*;

public class DbInitializer
{
    private static Connection connection;

    private DbInitializer()   // Singleton
    {
    }

    public static Connection getConnection()
    {
        return connection;
    }

    public static void connectToDerbyDB() throws SQLException
    {
        String dbConnectionString = "jdbc:derby:derbyMessagingDB;create=true";  // Creates the database if it doesn't exist
        connection = DriverManager.getConnection(dbConnectionString);
    }

    public static void validateDatabase() throws SQLException
    {
        if (!isDatabaseSetup())
        {
            Statement statement = connection.createStatement();

            String createUsersTable =
                "create table USERS (" +
                    "USER_ID varchar(50) primary key, " +
                    "FIRST_NAME varchar(50), " +
                    "LAST_NAME varchar(50))";
            statement.executeUpdate(createUsersTable);

            String createMessagesTable =
                "create table MESSAGES (" +
                    "MESSAGE_ID int primary key generated always as identity (start with 1, increment by 1), " +
                    "SENDER_ID varchar(50), " +
                    "RECEIVER_ID varchar(50), " +
                    "SENT_DATE timestamp , " +
                    "MESSAGE_TEXT varchar(1000))";
            statement.executeUpdate(createMessagesTable);

            addSomeData();
        }
    }

    public static void addSomeData() throws SQLException
    {
        Statement statement = connection.createStatement();

        statement.executeUpdate("insert into USERS values ('knatte', 'knatte', 'anka')");
        statement.executeUpdate("insert into USERS values ('fnatte', 'fnatte', 'anka')");
        statement.executeUpdate("insert into USERS values ('tjatte', 'tjatte', 'anka')");

        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('knatte', 'fnatte', '2001-12-01 15:36:00', 'Fika?')");
        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('knatte', 'tjatte', '2007-12-01 15:36:00', 'Gärna!')");
        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('fnatte', 'knatte', '2009-12-01 15:36:00', 'Bomber och granater!')");
        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('tjatte', 'knatte', '2010-12-02 15:36:00', 'Anfäkta och anamma!')");
        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('knatte', 'fnatte', '2011-12-03 15:36:00', 'Anamma och regera!')");
        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('knatte', 'tjatte', '2012-12-04 15:36:00', 'God morgon')");
        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('fnatte', 'knatte', '2013-12-05 15:36:00', 'Goddag')");
        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('fnatte', 'tjatte', '2014-12-06 15:36:00', 'God eftermiddag')");
        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('knatte', 'tjatte', '2015-12-07 15:36:00', 'Godkväll')");
        statement.executeUpdate("insert into MESSAGES (SENDER_ID, RECEIVER_ID, SENT_DATE, MESSAGE_TEXT) " +
            "values ('fnatte', 'knatte', '2016-12-08 15:36:00', 'Godnatt')");
    }

    private static boolean isDatabaseSetup() throws SQLException
    {
        DatabaseMetaData metaData = connection.getMetaData();
        ResultSet tables = metaData.getTables(null, null, "USERS", null);   // Check whether table USERS exists...
        return tables.next();
    }
}
