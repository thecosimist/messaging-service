package com.thecosimist.messaging.server;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class Init
{
    public static void main(String[] args) throws Exception
    {
        DbInitializer.connectToDerbyDB();
        DbInitializer.validateDatabase();

        ServletContextHandler baseContext = new ServletContextHandler();
        baseContext.addServlet(DefaultServlet.class, "/");

        ServletContextHandler restContext = new ServletContextHandler();
        restContext.setContextPath("/rest");
        ServletHolder restHolder = restContext.addServlet(ServletContainer.class, "/*");
        restHolder.setInitParameter("jersey.config.server.provider.classnames", RestServlet.class.getCanonicalName());

        ContextHandlerCollection handlers = new ContextHandlerCollection();
        handlers.setHandlers(new Handler[]{baseContext, restContext});

        Server server = new Server(8081);
        server.setHandler(handlers);

        server.start();
        server.join();
    }
}
