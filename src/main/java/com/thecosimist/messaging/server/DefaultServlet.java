/**
 * Created with IntelliJ IDEA.
 * User: cosimospada
 * Date: 2016-05-29
 * Time: 23:10
 */
package com.thecosimist.messaging.server;

import org.eclipse.jetty.http.HttpStatus;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

// Serves a regular web page with a single page HTML5 app
public class DefaultServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException
    {
//        RequestDispatcher welcomePage = req.getRequestDispatcher("/src/resources/basicGui");
//        welcomePage.forward(req, resp);
//        resp.setStatus(HttpStatus.OK_200);
//        resp.getWriter().println("Looking good!");

        BufferedReader buff = new BufferedReader(new FileReader("src/resources/basicGui.html"));
        StringBuilder basicGuiHtml = new StringBuilder();
        String readLine = buff.readLine();
        while (readLine != null)
        {
            basicGuiHtml.append(readLine);
            basicGuiHtml.append("\n");

            readLine = buff.readLine();
        }

        buff.close();

        resp.setStatus(HttpStatus.OK_200);
        resp.getWriter().println(basicGuiHtml.toString());
    }
}
