package com.thecosimist.messaging.rest;

import com.thecosimist.messaging.server.DbInitializer;
import com.thecosimist.messaging.server.RestServlet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class RestServletTest
{
    // create test suite?

    // Set up API tests
    // Set up integration tests with server running

    @Before
    public void init()
    {
        System.out.println("hey, starting");
        try
        {
            DbInitializer.connectToDerbyDB();
            DbInitializer.validateDatabase();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void testApp()
    {
        RestServlet restServlet = new RestServlet();
        Response response = restServlet.getMessagesSince("1", "2005-12-01 15:00:55");
        assertEquals(1, 1);
    }

    @After
    public void cleanUp()
    {
        System.out.println("hey, ending");
    }
}
