package com.thecosimist.messaging;

import com.thecosimist.messaging.rest.RestServletTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
    RestServletTest.class
})
public class TestSuite
{
 /* empty class */
}